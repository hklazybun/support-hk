import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Listing from "../../components/listing"

const Restaurants = () => (
  <StaticQuery
    query={graphql`
      query RestaurantsQuery {
        allAirtable(
          filter: { table: { eq: "RestaurantsBoycott" } }
          sort: { fields: data___name }
        ) {
          nodes {
            data {
              tel
              nameEN
              name
              addressEN
              address
              note
              locX
              locY
            }
          }
        }
      }
    `}
    render={data => <Listing data={data} />}
  />
)

export default Restaurants
