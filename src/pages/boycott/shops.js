import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Listing from "../../components/listing"

const Shops = () => (
  <StaticQuery
    query={graphql`
      query shopsQuery {
        allAirtable(
          filter: { table: { eq: "ShopsBoycott" } }
          sort: { fields: data___name }
        ) {
          nodes {
            data {
              tel
              note
              locX
              locY
              nameEN
              name
              addressEN
              address
            }
          }
        }
      }
    `}
    render={data => <Listing data={data} />}
  />
)

export default Shops
