import React, { useState } from "react"
import styled from "styled-components"
import media from "styled-media-query"

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr;
  border-bottom: 1px solid lightgrey;
  transition: all 0.3s;
  ${media.lessThan("medium")`
    grid-template-columns: 1fr;
    border-color: black;
    padding-top: 1rem;
    padding-bottom: 1rem;
  `}
`
const Basic = styled.div`
  border-right: 1px solid lightgrey;
  padding-top: 6px;
  padding-bottom: 6px;
  ${media.lessThan("medium")`
  border-right: none;
  `}
`

const Detail = styled.div`
  padding-right: 3rem;
  word-break: break-all;
  padding-left: 1rem;
  padding-top: 6px;
  padding-bottom: 6px;
  ${media.lessThan("medium")`
    padding-left: 0;
  `}
`

const Name = styled.div`
  h3 {
    margin: 0;
  }
  ${media.lessThan("medium")`
    font-size: 1.125rem;
    background-color: lightgrey;
  `}
`

const Item = ({ data }) => {
  const [show, setShow] = useState(true)

  if (show)
    return (
      <Container>
        <Basic>
          <Name>
            <h3>{data.name}</h3>
          </Name>
          <Name>
            <h3>
              {data.name !== data.nameEN && data.nameEN !== "" && data.nameEN}
            </h3>
          </Name>
          <br />
          <div>{data.address}</div>
          <div>{data.addressEN}</div>
          <div>{data.tel}</div>
        </Basic>
        <Detail>
          <div dangerouslySetInnerHTML={{ __html: data.note }}></div>
        </Detail>
      </Container>
    )
  else return <></>
}
export default Item
