import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

const Wrapper = styled.div`
  display: flex;
  margin-bottom: 1rem;
`

const Option = styled.div`
  font-size: 1.5rem;
  a {
    padding: 0.75rem 1rem;
  }
  ${".active"} {
    background: lightblue;
  }
`

const Nav = () => (
  <Wrapper>
    <Option>
      <Link activeClassName="active" to="/boycott/restaurants/">
        罷食
      </Link>
    </Option>
    <Option>
      <Link activeClassName="active" to="/boycott/shops">
        罷買
      </Link>
    </Option>
  </Wrapper>
)

export default Nav
