import React, { useState } from "react"
import Item from "./item"
import Layout from "../components/layout"
import styled from "styled-components"
import uuid from "uuid"

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const Input = styled.div`
  display: flex;
  margin: 2rem 0;
  input {
    font-size: 1.5rem;
    width: 100%;
  }
`

const Listing = ({ data }) => {
  const items = data.allAirtable.nodes
  const [value, setValue] = useState("")
  const handleChange = event => {
    setValue(event.target.value)
  }

  if (value !== "") {
    for (let [index, item] of items.entries()) {
      const exist = Object.keys(item.data).some(key => {
        if (item.data[key] && typeof item.data[key] === "string")
          return item.data[key].toLowerCase().includes(value.toLowerCase())
        return false
      })
      if (exist) {
        items[index]["hide"] = false
      } else {
        items[index]["hide"] = true
      }
    }
  } else {
    for (let item of items) {
      item["hide"] = false
    }
  }

  return (
    <Layout>
      <Input>
        <label></label>
        <input
          placeholder="Search"
          type="text"
          value={value}
          onChange={handleChange}
        />
      </Input>
      <Wrapper>
        {items.map(
          item => !item.hide && <Item key={uuid.v4()} data={item.data} />
        )}
      </Wrapper>
    </Layout>
  )
}

export default Listing
