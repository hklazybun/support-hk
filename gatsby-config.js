require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  pathPrefix: `/support-hk`,
  siteMetadata: {
    title: `Anti-BlueRibbons`,
    description: `:o)`,
    author: ``,
  },
  plugins: [
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-airtable`,
      options: {
        apiKey: `${process.env.AIRTABLE_API_KEY || process.env.GATSBY_API_KEY}`, // may instead specify via env, see below
        tables: [
          {
            baseId: `${process.env.AIRTABLE_BASEID ||
              process.env.GATSBY_BASE_ID}`,
            tableName: `RestaurantsBoycott`,
          },
          {
            baseId: `${process.env.AIRTABLE_BASEID ||
              process.env.GATSBY_BASE_ID}`,
            tableName: `RestaurantsToGo`,
          },
          {
            baseId: `${process.env.AIRTABLE_BASEID ||
              process.env.GATSBY_BASE_ID}`,
            tableName: `ShopsBoycott`,
          },
          {
            baseId: `${process.env.AIRTABLE_BASEID ||
              process.env.GATSBY_BASE_ID}`,
            tableName: `ShopsToGo`,
          },
          // {
          //   baseId: `YOUR_AIRTABLE_BASE_ID`,
          //   tableName: `YOUR_TABLE_NAME`,
          //   tableView: `YOUR_TABLE_VIEW_NAME`, // optional
          //   // can leave off queryName, mapping or tableLinks if not needed
          // },
        ],
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
